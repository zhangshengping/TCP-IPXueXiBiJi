
# 术语

## 术语和定义

| **术语** | **定义** |
|:----:|:---------- |
| Anycast Locator(ALOC)| ALOC 是一个IPv6的任播地址 |
| Border Router | 任何具有在Thread和非Thread网络间传输的设备。如果Commissioner在一个非Thread网络，Border Router也扮演Commissioner接口点的角色。Border Router需要一个Thread接口，也可以同时扮演Thread网络中Joiner外其它角色 |
| Commissioner | 当前被选中为设备接入提供授权和认证服务的设备。有可能被选作为Commissioner的设备被称作Commissioner Candidate。没有Thread接口的设也能作为Commissioner。除Commissioner角色外，设备也可以包含其它的功能。|
| Commissioner Candidate | 有能力成为Commissioner的设备，但当前不是Commissioner。 |
| End Device(ED) | 仅与父设备实现单播传输的thread设备。FTD可能是一个REED或FED，MTD可能是MED或者SED。 | 
| Endpoint Identifier(EID)  | EID是Thread网域内唯一标识Thread接口的一个IPv6地址，EID不因网络拓扑结构的改变而变化 |
| External route | 内外IPv6包通过border router到达外部网络需要跨过的路由器 |
| Frame counter | ~~每个新的安全消息该值都会增加~~，用于检测消息重传 |
| Full End Device(FED) | 纯粹的终端设备， FED只能扮演终端设备的角色 |
| Full Thread Device(FTD) | FTD可以扮演路由、REED、FED。不像MTD，FTD保持着与相邻路由的连接 |
| Interior network | ~~？？~~ |
| Joiner | ?? |
| Joiner Router | ?? |
| Leader | 管理路由ID分配的设备 |
| Link Cost | ?? |
| Link Quality | ?? |







